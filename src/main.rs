#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate diesel;

use actix_cors::Cors;
use actix_web::{App, HttpServer};
use dotenv::dotenv;
use std::env;

// module declaration.
mod api_config;
mod db_manager;
mod health_checking;
mod log4rs_extensions;
mod logger;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    // enable .env
    dotenv().ok();

    // get environment variables.
    let listen_to_addr = env::var("BINDING_ADDR").expect("BINDING_ADDR must be set");
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let log_path = env::var("LOG_CONFIG_PATH").expect("LOG_CONFIG_PATH must be set");

    // init logger
    logger::init(&log_path);

    info!("BINDING_ADDR: {}", &listen_to_addr);
    info!("DATABASE_URL: {}", &db_url);
    info!("LOG_PATH: {}", &log_path);

    // create connection pool of database
    let pool = db_manager::create_connection_pool(&db_url);

    // create a http server
    HttpServer::new(move || {
        App::new()
            // allow CORS for ui development. Consider to disable this on production.
            .wrap(Cors::new().finish())
            // attach db connection pool to data of actix.
            .data(pool.clone())
            // configure actix route via api_config module.
            .configure(api_config::config)
    })
    .bind(&listen_to_addr)?
    .run()
    .await
}
