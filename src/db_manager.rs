use diesel::r2d2::ConnectionManager;
use diesel::r2d2::Pool;
use diesel::{r2d2, PgConnection};

pub fn create_connection_pool(db_url: &str) -> Pool<ConnectionManager<PgConnection>> {
    // create a connection to the db
    let connection_manager = ConnectionManager::<PgConnection>::new(db_url);

    // create a connection pool
    r2d2::Pool::builder()
        .build(connection_manager)
        .expect("Failed to create pool.")
}
