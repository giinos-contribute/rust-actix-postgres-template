use crate::health_checking::HealthCheck;

/// Get version number from Cargo.toml
pub fn get_health_status() -> HealthCheck {
    let version = env!("CARGO_PKG_VERSION");

    HealthCheck {
        app_version: version.into(),
    }
}
