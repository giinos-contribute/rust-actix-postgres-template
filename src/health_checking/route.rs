use crate::health_checking::handler;
use actix_web::{get, HttpResponse};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[get("")]
pub fn check_health_status() -> HttpResponse {
    let health_status = handler::get_health_status();
    let payload = serde_json::to_value(health_status).expect("Could not parse to json");

    HttpResponse::Ok().json(payload)
}
