pub mod handler;
pub mod route;

#[derive(Debug, Serialize, Deserialize)]
pub struct HealthCheck {
    app_version: String,
}
