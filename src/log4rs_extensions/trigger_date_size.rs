use serde::de;
use std::error::Error;
use std::fmt;
use std::fs;
use std::time::SystemTime;

use log4rs::append::rolling_file::policy::compound::trigger::Trigger;
use log4rs::append::rolling_file::LogFile;
use log4rs::file::{Deserialize, Deserializers};
use std::collections::HashMap;
use std::sync::Mutex;

/// Configuration for the size trigger.
#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DateSizeTriggerConfig {
    #[serde(deserialize_with = "deserialize_limit")]
    limit: u64,
}

fn deserialize_limit<'de, D>(d: D) -> Result<u64, D::Error>
where
    D: de::Deserializer<'de>,
{
    struct V;

    impl<'de2> de::Visitor<'de2> for V {
        type Value = u64;

        fn expecting(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
            fmt.write_str("a size")
        }

        fn visit_u64<E>(self, v: u64) -> Result<u64, E>
        where
            E: de::Error,
        {
            Ok(v)
        }

        fn visit_i64<E>(self, v: i64) -> Result<u64, E>
        where
            E: de::Error,
        {
            if v < 0 {
                return Err(E::invalid_value(
                    de::Unexpected::Signed(v),
                    &"a non-negative number",
                ));
            }

            Ok(v as u64)
        }

        fn visit_str<E>(self, v: &str) -> Result<u64, E>
        where
            E: de::Error,
        {
            let (number, unit) = match v.find(|c: char| !c.is_digit(10)) {
                Some(n) => (v[..n].trim(), Some(v[n..].trim())),
                None => (v.trim(), None),
            };

            let number = match number.parse::<u64>() {
                Ok(n) => n,
                Err(_) => return Err(E::invalid_value(de::Unexpected::Str(number), &"a number")),
            };

            let unit = match unit {
                Some(u) => u,
                None => return Ok(number),
            };

            let number = if unit.eq_ignore_ascii_case("b") {
                Some(number)
            } else if unit.eq_ignore_ascii_case("kb") || unit.eq_ignore_ascii_case("kib") {
                number.checked_mul(1024)
            } else if unit.eq_ignore_ascii_case("mb") || unit.eq_ignore_ascii_case("mib") {
                number.checked_mul(1024 * 1024)
            } else if unit.eq_ignore_ascii_case("gb") || unit.eq_ignore_ascii_case("gib") {
                number.checked_mul(1024 * 1024 * 1024)
            } else if unit.eq_ignore_ascii_case("tb") || unit.eq_ignore_ascii_case("tib") {
                number.checked_mul(1024 * 1024 * 1024 * 1024)
            } else {
                return Err(E::invalid_value(de::Unexpected::Str(unit), &"a valid unit"));
            };

            match number {
                Some(n) => Ok(n),
                None => Err(E::invalid_value(de::Unexpected::Str(v), &"a byte size")),
            }
        }
    }

    d.deserialize_any(V)
}

/// A trigger which rolls the log once it has passed a certain size.
#[derive(Debug)]
pub struct DateSizeTrigger {
    limit: u64,
}

impl DateSizeTrigger {
    /// Returns a new trigger which rolls the log once it has passed the
    /// specified size in bytes.
    pub fn new(limit: u64) -> DateSizeTrigger {
        DateSizeTrigger { limit }
    }
}

lazy_static! {
    static ref LAST_MODIFIED_DAYS: Mutex<HashMap<String, u64>> = Mutex::new(HashMap::new());
}

impl Trigger for DateSizeTrigger {
    fn trigger(&self, file: &LogFile) -> Result<bool, Box<Error + Sync + Send>> {
        let path = file
            .path()
            .to_str()
            .expect("LogTrigger: Get path failed")
            .to_owned();
        let today_days = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("LogTrigger: Convert current time failed")
            .as_secs()
            / 86400;
        let mut changed = false;
        let last_modified_days = match LAST_MODIFIED_DAYS
            .lock()
            .expect("LogTrigger: Lock last modified days failed")
            .get(&path)
        {
            Some(last_modified_days) => last_modified_days.clone(),
            None => {
                changed = true;
                let metadata = fs::metadata(file.path()).expect("LogTrigger: Get meta data failed");
                match metadata.created() {
                    Ok(created) => {
                        created
                            .duration_since(SystemTime::UNIX_EPOCH)
                            .expect("LogTrigger: Convert created time failed")
                            .as_secs()
                            / 86400
                    }
                    Err(_e1) => match metadata.modified() {
                        Ok(modified) => {
                            modified
                                .duration_since(SystemTime::UNIX_EPOCH)
                                .expect("LogTrigger: Convert modified time failed")
                                .as_secs()
                                / 86400
                        }
                        Err(_e2) => today_days,
                    },
                }
            }
        };

        if today_days > last_modified_days {
            LAST_MODIFIED_DAYS
                .lock()
                .expect("LogTrigger: Lock last modified days failed")
                .insert(path, today_days);
            Ok(true)
        } else {
            if changed {
                LAST_MODIFIED_DAYS
                    .lock()
                    .expect("LogTrigger: Lock last modified days failed")
                    .insert(path, last_modified_days);
            }
            Ok(file.len() > self.limit)
        }
    }
}

/// A deserializer for the `SizeTrigger`.
///
/// # Configuration
///
/// ```yaml
/// kind: size
///
/// # The size limit in bytes. The following units are supported (case insensitive):
/// # "b", "kb", "kib", "mb", "mib", "gb", "gib", "tb", "tib". The unit defaults to
/// # bytes if not specified. Required.
/// limit: 10 mb
/// ```
pub struct DateSizeTriggerDeserializer;

impl Deserialize for DateSizeTriggerDeserializer {
    type Trait = Trigger;

    type Config = DateSizeTriggerConfig;

    fn deserialize(
        &self,
        config: DateSizeTriggerConfig,
        _: &Deserializers,
    ) -> Result<Box<Trigger>, Box<Error + Sync + Send>> {
        Ok(Box::new(DateSizeTrigger::new(config.limit)))
    }
}
