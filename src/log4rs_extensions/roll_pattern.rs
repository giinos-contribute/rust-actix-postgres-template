use std::env;
use std::error::Error;
use std::fs;
use std::io;
use std::path::Path;
use std::time::SystemTime;

use chrono::{Local, TimeZone, Utc};
use log4rs::append::rolling_file::policy::compound::roll::Roll;
use log4rs::file::{Deserialize, Deserializers};

/// Configuration for the fixed window roller.
#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DateTimeRollerConfig {
    pattern: String,
    base: Option<u32>,
    days_limit: Option<i64>,
}

#[derive(Debug)]
enum Compression {
    None,
    Gzip,
}

impl Compression {
    fn compress(&self, src: &Path, dst: &str) -> io::Result<()> {
        match *self {
            Compression::None => move_file(src, dst),
            Compression::Gzip => {
                use flate2;
                use flate2::write::GzEncoder;
                use std::fs::File;

                let mut i = File::open(src)?;

                let o = File::create(dst)?;
                let mut o = GzEncoder::new(o, flate2::Compression::default());

                io::copy(&mut i, &mut o)?;
                drop(o.finish()?);
                drop(i); // needs to happen before remove_file call on Windows

                fs::remove_file(src)
            }
        }
    }
}

#[derive(Debug)]
pub struct DateTimeRoller {
    pattern: String,
    compression: Compression,
    base: u32,
    days_limit: i64,
}

impl DateTimeRoller {
    /// Returns a new builder for the `DateTimeRoller`.
    pub fn builder() -> DateTimeRollerBuilder {
        DateTimeRollerBuilder {
            base: 0,
            days_limit: 90,
        }
    }
}

impl Roll for DateTimeRoller {
    fn roll(&self, file: &Path) -> Result<(), Box<std::error::Error + Sync + Send>> {
        let metadata = fs::metadata(file).expect("LogRoll: Get meta data failed");
        let last_modified_sys_time = match metadata.created() {
            Ok(created) => created
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("LogRoll: Convert created time failed")
                .as_secs(),
            Err(_e1) => match metadata.modified() {
                Ok(modified) => modified
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .expect("LogRoll: Convert modified time failed")
                    .as_secs(),
                Err(_e2) => SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .expect("LogRoll: Convert current time failed")
                    .as_secs(),
            },
        };
        let dst_0 = self.pattern.replace(
            "{d}",
            &Local
                .timestamp(last_modified_sys_time as i64, 0)
                .format("%Y-%m-%d")
                .to_string(),
        );
        let mut dst = dst_0.replace("{i}", &self.base.to_string());

        if let Some(parent) = Path::new(&dst).parent() {
            fs::create_dir_all(parent)?;
        }

        // In the common case, all of the archived files will be in the same
        // directory, so avoid extra filesystem calls in that case.
        let _parent_varies = match (Path::new(&dst).parent(), Path::new(&self.pattern).parent()) {
            (Some(a), Some(b)) => a != b,
            _ => false, // Only case that can actually happen is (None, None)
        };

        let mut i = self.base;
        loop {
            if !Path::new(&dst).exists() {
                break;
            }
            i += 1;
            dst = dst_0.replace("{i}", &i.to_string());
        }

        let log_path = env::var("LOG_PATH").expect("LOG_PATH must bee set");
        let file_in_dir = get_file_in_dir_full_path(&log_path);
        for full_path in file_in_dir {
            let file_name = get_file_name(&full_path);
            let duration = get_duration_of_log_file(file_name);
            if duration > self.days_limit {
                fs::remove_file(&full_path).expect("ERROR at remove file");
            }
        }

        //move_file(file, &dst).map_err(Into::into)
        self.compression.compress(file, &dst).map_err(Into::into)
    }
}

pub struct DateTimeRollerDeserializer;

impl Deserialize for DateTimeRollerDeserializer {
    type Trait = Roll;

    type Config = DateTimeRollerConfig;

    fn deserialize(
        &self,
        config: DateTimeRollerConfig,
        _: &Deserializers,
    ) -> Result<Box<Roll>, Box<std::error::Error + Sync + Send>> {
        let mut builder = DateTimeRoller::builder();
        if let Some(base) = config.base {
            builder = builder.base(base);
        }

        if let Some(days_limit) = config.days_limit {
            builder = builder.days_limit(days_limit);
        }

        Ok(Box::new(builder.build(&config.pattern)?))
    }
}

/// A builder for the `FixedWindowRoller`.
pub struct DateTimeRollerBuilder {
    base: u32,
    days_limit: i64,
}

impl DateTimeRollerBuilder {
    /// Sets the base index for archived log files.
    ///
    /// Defaults to 0.
    pub fn base(mut self, base: u32) -> DateTimeRollerBuilder {
        self.base = base;
        self
    }

    pub fn days_limit(mut self, days_limit: i64) -> DateTimeRollerBuilder {
        self.days_limit = days_limit;
        self
    }

    /// Constructs a new `FixedWindowRoller`.
    ///
    /// `pattern` must contain at least one instance of `{}`, all of which will
    /// be replaced with an archived log file's index.
    ///
    /// If the file extension of the pattern is `.gz` and the `gzip` Cargo
    /// feature is enabled, the archive files will be gzip-compressed.
    pub fn build(
        self,
        pattern: &str,
    ) -> Result<DateTimeRoller, Box<std::error::Error + Sync + Send>> {
        if !pattern.contains("{i}") || !pattern.contains("{d}") {
            return Err("pattern does not contain `{i}` or `{d}`".into());
        }

        let compression = match Path::new(pattern).extension() {
            Some(e) if e == "gz" => Compression::Gzip,
            _ => Compression::None,
        };

        Ok(DateTimeRoller {
            pattern: pattern.to_owned(),
            compression: compression,
            base: self.base,
            days_limit: self.days_limit,
        })
    }
}

fn move_file<P>(src: P, dst: &str) -> io::Result<()>
where
    P: AsRef<Path>,
{
    // first try a rename
    match fs::rename(src.as_ref(), dst) {
        Ok(()) => return Ok(()),
        Err(ref e) if e.kind() == io::ErrorKind::NotFound => return Ok(()),
        Err(_) => {}
    }

    // fall back to a copy and delete if src and dst are on different mounts
    fs::copy(src.as_ref(), dst).and_then(|_| fs::remove_file(src.as_ref()))
}

fn get_file_in_dir_full_path(dir_path: &str) -> Vec<String> {
    let mut full_path = Vec::new();
    for elem in fs::read_dir(dir_path).expect("Doesn't have any log") {
        let path = elem.expect("Dose have any file or directory").path();
        let full_path_str = format!("{}", path.to_string_lossy());
        full_path.push(full_path_str);
    }
    full_path
}

fn get_file_name(path: &str) -> &str {
    let file = path.split("/");
    let elem_of_file: Vec<&str> = file.collect();
    let file_name = elem_of_file[elem_of_file.len() - 1];
    file_name
}

fn get_duration_of_log_file(file: &str) -> i64 {
    let split_file_name = file.split(|c| c == '-' || c == '.');
    let elem_of_file_name: Vec<&str> = split_file_name.collect();
    println!("Work on this file: >>>> {:?}", elem_of_file_name);
    if elem_of_file_name.len() > 2 {
        let y = elem_of_file_name[1]
            .parse::<i32>()
            .expect("Could not parse year");
        let m = elem_of_file_name[2]
            .parse::<u32>()
            .expect("Could not parse month");
        let d = elem_of_file_name[3]
            .parse::<u32>()
            .expect("Could not parse date");
        let file_date = Local.ymd(y, m, d);
        let now = Local::today();
        let dura = now.signed_duration_since(file_date).num_days();
        println!("{}", dura);
        dura
    } else {
        println!("This is not a gz file: {:?}", elem_of_file_name);
        -1
    }
}
