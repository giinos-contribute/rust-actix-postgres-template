use crate::health_checking::route::check_health_status;
use actix_web::web;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        // do health checking
        web::scope("/health").service(check_health_status),
    );
}
