use crate::log4rs_extensions::roll_pattern::DateTimeRollerDeserializer;
use crate::log4rs_extensions::trigger_date_size::DateSizeTriggerDeserializer;

pub fn init(log_path: &str) {
    let mut log_deserializer: log4rs::file::Deserializers = Default::default();
    log_deserializer.insert("date_time", DateTimeRollerDeserializer);
    log_deserializer.insert("date_size", DateSizeTriggerDeserializer);

    log4rs::init_file(log_path, log_deserializer).expect("Could not init log");
}
