# rust-actix-postgres-template

This is template project used with **cargo generate** command (please see more information [here]( https://github.com/ashleygwilliams/cargo-generate) )

#### Usage

`cargo generate --git https://gitlab.com/giinos-contribute/rust-actix-postgres-template.git`

then specify your project name.

#### Install dependencies libraries.
1. Install libpq
`sudo apt update && sudo apt install libpq-dev`

#### Preparation before running the service.

1. Create postgres database named "my-db" 
2. Change your username / password to access postgres in .env file.

```
# database connection string
DATABASE_URL=postgres://postgres:postgres@localhost:5433/my-db
```

#### Run

1. Run application by using `cargo run`
2. visit http://localhost:2233/health to check that the service is up&running.

#### This template included following features.

- Actix-web framework for RESTFUL Api service.
- Log4rs with rolling file configuration. 
- Diesel for connecting to Postgresql database with r2d2 connection pool configuration.






 